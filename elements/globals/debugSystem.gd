extends Node

var MODES = [ "NONE", "PATHS" , "COLLISION"]

var debug = MODES[0]

signal debugMode

func nextDebugMode():
	debug = MODES[(MODES.find(debug) + 1) % MODES.size() ]
	print("Debug Mode: ", debug)
	var tree = get_tree()
	tree.debug_navigation_hint = (debug == "PATHS")
	tree.debug_collisions_hint = (debug == "COLLISION")
#	tree.reload_current_scene()

func _process(delta):
	if Input.is_action_just_pressed("debug"):
		nextDebugMode()
		emit_signal("debugMode",debug)
	if Input.is_action_just_pressed("reload"):
		get_tree().reload_current_scene()
	
