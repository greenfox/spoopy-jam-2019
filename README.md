# Spoopy Game Jam 2019

We're doing a twin stick zombie shooter. This is an Isometric Game.

Hopefully, we'll have time to add multiplayer!

# Links

## [Prototype Page](https://greenfox.gitlab.io/spoopy-jam-2019/)

## [Web Version](https://greenfox.gitlab.io/spoopy-jam-2019/HTML5/Spoopy2019.html)